function [TrainIdx, TestIdx] = dataset_creation(labels, varargin)
% [TrainIdx, TestIdx] = dataset_creation(labels, ['Train', NT, 'Type', 'type', 'N', NFolds)
%
% Given a vector of labels, it create two vector index for training set and
% for test set.
%
% - labels              vector with trial labels
% - 'Train', NT         Percentile of trial for training. The rest is used
%                       for test set
% - 'Type', 'type'      'temporal' -> select the percentile for the
%                       training and testing set in temporal order
%                       'random' -> select the percentile randomly
%                       'kfold', 'N', NFolds  -> create k-fold indexes
%
% It returns boolean vector indexes for train and test set [NTrials x 1]. In case of
% cross-validation, it returns a matrix [NTrials x Fold]

    pnames = {'Train' 'Type', 'N'};
    default  = {80 'temporal' 5};
    [eid,emsg, TrainPerc, type, NFolds] = util_getargs(pnames, default ,varargin{:});
    
    
    if strcmpi(type, 'temporal')
        [TrainIdx, TestIdx] = temporal_dataset(labels, TrainPerc);
    elseif strcmpi(type, 'random')
        [TrainIdx, TestIdx] = random_dataset(labels, TrainPerc);
    elseif strcmpi(type, 'kfold')
        [TrainIdx, TestIdx] = kfold_dataset(labels, NFolds);
    end
        

end

function [TrainIdx, TestIdx] = temporal_dataset(labels, TrainPerc)
    
    Classes = unique(labels);
    NumClasses = length(Classes);
    NumTrials = length(labels);
    
    TrainIdx = false(NumTrials, 1);
    TestIdx  = false(NumTrials, 1);
    
    train_id = [];
    
    for c = 1:NumClasses
        ClassId = labels == Classes(c);
        NumberClassTrial = sum(labels == Classes(c));  
        NumberClassTrain = TrainPerc*NumberClassTrial/100;
        
        train_id = sort([train_id; find(ClassId == 1, ceil(NumberClassTrain))]);
        
    end
    
    TrainIdx(train_id) = true;
    TestIdx = ~TrainIdx;
    
end

function [TrainIdx, TestIdx] = random_dataset(labels, TrainPerc)

    Classes = unique(labels);
    NumClasses = length(Classes);
    NumTrials = length(labels);
    
    
    NumberTrial = [0 0];
    NumberTrain = [0 0];
    ClassTrial  = false(length(labels), 2);
    train_id = [];
    TrainIdx = false(length(labels), 1);
    
    for c = 1:NumClasses
        ClassId = find(labels == Classes(c));
        NumberClassTrial = sum(labels == Classes(c));  
        
        RandId = ClassId(randperm(NumberClassTrial));
        
        NumberClassTrain = ceil(TrainPerc*NumberClassTrial/100);
              
        
        train_id = sort([train_id; RandId(1:NumberClassTrain)]);
        
    end
    
    TrainIdx(train_id) = true;
    TestIdx = ~TrainIdx;
    
end

function [TrainIdx, TestIdx] = kfold_dataset(labels, NFolds)
    
    TrainIdx = false(length(labels), NFolds);
    TestIdx = false(length(labels), NFolds);

    indices = crossvalind('Kfold', labels, NFolds);
    
    for i = 1:NFolds

        curr_test = indices == i;
        curr_train = ~curr_test;

        TrainIdx(:, i) = curr_train;
        TestIdx(:, i)  = curr_test;

    end


end